const mongoose = require('mongoose');

const SpotSchema = new mongoose.Schema({
    thumbnail: String,
    company: String,
    price: Number,
    techs: [String],
    user: {
        type: mongoose.Schema.Types.ObjectID,
        ref: 'User',
    },
}, {
    toJSON: {
        virtuals: true,
    },
});

SpotSchema.virtual('thumbnail_url').get(function () {
    return `http://api.tijack.com.br/files/${ this.thumbnail }`;
});

module.exports = mongoose.model('Spot', SpotSchema);