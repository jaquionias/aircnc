const express = require('express');
const http = require('http');
const socketIO = require('socket.io');

const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');
const routes = require('./routes');

const PORT = 3333;

const app = express();


const server = http.createServer(app);

const io = socketIO(server);

const connectedUsers = {};

mongoose.connect('mongodb+srv://jcastro:omnistack@cluster0.lslps.mongodb.net/semana09?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

io.on('connection', socket => {
    const { user_id } = socket.handshake.query;

    connectedUsers[user_id] = socket.id;
});

app.use((req, res, next) => {
    req.io = io;
    req.connectedUsers = connectedUsers;

    return next();
});


app.use(cors());
app.use(express.json());
app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads')));
app.use(routes);

server.listen(PORT, function () {
        console.log('listening on port ' + PORT);
    },
);