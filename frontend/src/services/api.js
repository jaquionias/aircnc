import axios from 'axios';

const api = axios.create({
    baseURL: 'http://api.tijack.com.br',
});

export default api;